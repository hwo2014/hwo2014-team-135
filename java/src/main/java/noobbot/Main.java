package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

//* For Json Parsing testing TestRaceParsing
import java.io.* ;
import java.nio.file.Paths ;
import java.nio.file.Files ;
//*/

import com.google.gson.Gson;

public class Main {
    public final boolean debug = true;
    
    public static void main(String... args) throws IOException {
	// NOTE: Sorry, it's kind of difficult to find were java starts so passed absolute file path here
	// TestRaceParsing("/home/jenea/Projects/hwo2014-team-135/java/target/ParsingRaceTest.json");

        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
       
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

	CarIdentifier carId = null;
	RaceData raceData = null;
	CarPosition[] carPositions = null;
        send(join);
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
		carPositions = gson.fromJson(msgFromServer.data.toString(), CarPosition[].class);
		if(debug)
		  System.out.println("CarPosition: " + msgFromServer.data.toString());
                send(new Throttle(0.68));
                send(new TurnRight());
                
            } else if (msgFromServer.msgType.equals("yourCar")){
		carId = gson.fromJson(msgFromServer.data.toString(), CarIdentifier.class);
		System.out.println("Name: " +carId.name + " Color: " + carId.color);	
	    } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
		raceData = gson.fromJson(msgFromServer.data.toString(), RaceData.class);
		if(debug)
		  System.out.println("Race data: " + msgFromServer.data.toString());
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private static void TestRaceParsing(String jsonPath) throws IOException {
      final Gson gson = new Gson();
      //Gson gson = new GsonBuilder().create();
      System.out.println("Race Json Parsing");
      String raceJsonContent = new String(Files.readAllBytes(Paths.get(jsonPath)));
      System.out.println("Content:");
      System.out.println(raceJsonContent);
      Race race = gson.fromJson(raceJsonContent, Race.class);
      System.out.println("Race obj: " + race == null ? "NULL" : "Race init");
      System.out.println("Race id: " + race.track == null ? "NULL" : (race.track.id == null ? "track id is null" : race.track.id));
      System.out.println("Pieces count: " + race.track.pieces == null ? "NULL" : race.track.pieces.length );
      System.out.println("Piece 2 switch: " + race.track.pieces[1].Switch);
      System.out.println("Start Point: " + race.track.startingPoint.angle);
      System.out.println("Cars count: " + race.cars.length);
      System.out.println("Race session laps: " + race.raceSession.laps);
      
      //System.out.println("Race id: " + race.track.id);
      //System.out.println("Race id: " + race.track.pieces.length);
      System.out.println("Race Json Parsing Completed");
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class RaceData {
  public Race race;
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
class TurnRight extends SendMsg {

    public TurnRight() {
    }

    @Override
    protected Object msgData() {
        return "Right";
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
