package noobbot;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Piece {
    public float length;
    @SerializedName("switch")
    public boolean Switch;
    public int radius;
    public float angle;
}
 
