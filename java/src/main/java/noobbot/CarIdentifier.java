package noobbot;

public class CarIdentifier {
    public final String name;
    public final String color;
    
    CarIdentifier(final String name, final String color) {
	this.name = name;
	this.color = color;
    }
} 
